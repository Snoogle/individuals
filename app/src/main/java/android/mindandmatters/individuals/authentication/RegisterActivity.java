package android.mindandmatters.individuals.authentication;

import android.app.Activity;
import android.content.Intent;
import android.mindandmatters.individuals.R;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends Activity {


    //Firebase Methods
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    //Users Object
    private Map<String, Object> user;

    //Grabbing ID's
    private EditText email;
    private EditText password;
    private Button registerButton;
    private TextView registerError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Intializing Firebase Authentication
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        //Grabbing View ID's
        email = findViewById(R.id.emailAddress);
        password = findViewById(R.id.password);
        registerButton = findViewById(R.id.register_button);
        registerError = findViewById(R.id.register_error);

        //on submit we create the record and add it to the database
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Storing data in text
                String emailData = email.getText().toString().toLowerCase();
                String passwordData = password.getText().toString();

                //Check fields if they are empty
                if(emailData.equals("") || passwordData.equals("")){
                    registerError.setText(getString(R.string.empty_fields_registration));
                }else{
                    registerError.setText("");
                    createAccount(emailData, passwordData);
                }
            }
        });

    }

    //call this method to begin the createAccount process
    public void createAccount(final String email, final String password){
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    // Sign in success, update UI with the signed-in user's information
                    FirebaseUser currentUser = mAuth.getCurrentUser();

                    //storing the user data in the database
                    user = new HashMap<>();
                    user.put("email", email);
                    user.put("password", password);
                    user.put("firstTime", true);

                    db.collection("users").document(email)
                            .set(user)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    //Start the login activity again
                                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(intent);
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    registerError = findViewById(R.id.register_error);
                                    registerError.setText(getString(R.string.register_error_auth));
                                }
                            });
                } else {
                    registerError = findViewById(R.id.register_error);
                    Log.w("log", "createUserWithEmail:failure", task.getException());
                }
            }
         });
    }
}