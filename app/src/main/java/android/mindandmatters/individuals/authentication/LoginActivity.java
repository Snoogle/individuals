package android.mindandmatters.individuals.authentication;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.mindandmatters.individuals.R;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class LoginActivity extends Activity {


    //Accessing Firebase Methods
    private FirebaseAuth mAuth;
    FirebaseFirestore db;
    FirebaseUser currentUser;

    //setting the introduction flag
    boolean firstTime = true;

    //General Purpose Intent
    private Intent intent;

    //View ID's
    private TextView signInError;
    private EditText email;
    private EditText password;
    private Button registerButton;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //initializing Firebase auth
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        //Grabbing View ID's
        email = findViewById(R.id.login_username);
        password = findViewById(R.id.login_password);
        registerButton = findViewById(R.id.register_button);
        loginButton = findViewById(R.id.login_button);
        signInError = findViewById(R.id.sign_in_error);

        //On submit sign the user in and switch activities.
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Grabbing data from EditText
                String emailData = email.getText().toString();
                String passwordData = password.getText().toString();

                //Check fields if they are empty
                if(emailData.equals("") || passwordData.equals("")){
                    signInError.setText(getString(R.string.empty_fields_login));
                }else{
                    signInError.setText("");
                    signIn(emailData, passwordData);
                }

            }
        });

        //register button changes activity to Register
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });

    }

    //check current status of login
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getCurrentUser();
    }

    public void signIn(final String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            //Determining if user is new
                            DocumentReference docRef = db.collection("users").document(email);
                            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @SuppressWarnings("ConstantConditions")
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        DocumentSnapshot document = task.getResult();
                                        if (document.exists()) {
                                            firstTime = document.getBoolean("firstTime");
                                        }
                                    }
                                }
                            });

                            // Sign in success, start the next activity
                            if (firstTime) {
                                intent = new Intent(getApplicationContext(), Introduction.class);
                                startActivity(intent);
                            } else {
                                intent = new Intent(getApplicationContext(), profileActivitiy.class);
                                startActivity(intent);
                            }
                        } else {
                            //If authentication fails for whatever reason display error message
                            signInError = findViewById(R.id.sign_in_error);
                            signInError.setText(R.string.authentication_error);
                        }
                    }
                });
    }
}



