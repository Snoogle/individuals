package android.mindandmatters.individuals.authentication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<String> tabNames = new ArrayList<>();
    private ArrayList<Fragment> fragment = new ArrayList<>();

    //Instantiating fragments
    private TabFragment1 fragment1 = new TabFragment1();
    private TabFragment2 fragment2 = new TabFragment2();
    private TabFragment3 fragment3 = new TabFragment3();



    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        tabNames.add("Messages");
        tabNames.add("Portfolio");
        tabNames.add("Map");
    }

    @Override
    public Fragment getItem(int position) {

        fragment.add(fragment2);
        fragment.add(fragment1);
        fragment.add(fragment3);

        //keeping track of which fragment is selected
        position = position + 1;
        Bundle bundle = new Bundle();
        bundle.putString("message", "Fragment: " + position);

        return fragment.get(position-1);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position){
        position = position + 1;
        return tabNames.get(position-1);
    }
}
