package android.mindandmatters.individuals.authentication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.mindandmatters.individuals.R;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;



/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment3 extends Fragment {

    MapView mMapView;
    private GoogleMap googleMap;

    public TabFragment3(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_fragment3, container, false);

        mMapView = rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                //Accessing location methods
                GpsLocationTracker mGpsLocationTracker = new GpsLocationTracker(getActivity());

                //Getting Long and Lat of the device and adding a marker
                if (mGpsLocationTracker.canGetLocation()){
                    double latitude = mGpsLocationTracker.getLatitude();
                    double longitude = mGpsLocationTracker.getLongitude();
                    Log.i("tab3", String.format("latitude: %s", latitude));
                    Log.i("tab3", String.format("longitude: %s", longitude));

                    //TODO check if user has accepted the permissions
                    try{
                        googleMap.setMyLocationEnabled(true);
                    }catch(SecurityException e){
                        Log.w("tab3", "PERMISSION DENIED");
                    }

                    // For dropping a marker at a point on the Map
                    LatLng location = new LatLng(latitude, longitude);
                    googleMap.addMarker(new MarkerOptions().position(location).title("This is you!").snippet("Soon I'll display all of you!"));

                    // For zooming automatically to the location of the marker
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(12).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
                else{
                    mGpsLocationTracker.showSettingsAlert();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

}
