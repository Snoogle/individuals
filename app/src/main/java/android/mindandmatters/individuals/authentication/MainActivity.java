package android.mindandmatters.individuals.authentication;

import android.app.Activity;
import android.content.Intent;
import android.mindandmatters.individuals.R;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Auto Starting Login Activity
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
