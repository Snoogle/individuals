package android.mindandmatters.individuals.authentication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.mindandmatters.individuals.R;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Introduction extends Activity {

    //Accessing Firebase Methods
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private FirebaseUser currentUser;

    //grabbing the textview
    private TextView answer;
    private TextView conversation;
    private Button next;

    //Keep track of how many questions were answered.
    private int count;
    private Map<String, Object> user;

    public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);

        //starting Firebase instance
        mAuth = FirebaseAuth.getInstance();

        //initiating the conversation
        conversation();
    }


    //get current user and set the username textview to that user's email.
    @Override
    protected void onStart(){
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        TextView userLogged = findViewById(R.id.user_logged_introduction);
        userLogged.setText(currentUser.getEmail());
    }

    private void conversation(){

        //initializing firebase
        currentUser = mAuth.getCurrentUser();
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        user = new HashMap<>();

        //Grabbing View ID's
        conversation = findViewById(R.id.conversation);
        answer = findViewById(R.id.introduction_answer);
        next = findViewById(R.id.introduction_next);

        //Tested questions
        final ArrayList<String> questions = new ArrayList<>();
        questions.add("What is your favourite animal?");
        questions.add("Your favourite song?");
        questions.add("How old are you?");
        questions.add("Why are you running?");
        questions.add("What do you think your grade will be?");

        //Setting count to 0 and set questions to count.
        count = 0;
        conversation.setText(questions.get(count));

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(answer.getText().toString().equals("")){
                    answer.setHint("ERROR");
                }else if(count < questions.size()-1){

                    //Storing answers locally and references them with email + 1
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString(currentUser.getEmail()+1, answer.getText().toString());
                    editor.apply();

                    //creating the hashmap to store new answers
                    user.put("answer" + count, answer.getText().toString());

                    //Storing answers online
                    db.collection("users").document(currentUser.getEmail().toLowerCase())
                            .update(user)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                }
                            });

                    //Moving onto the next Question
                    count++;
                    conversation.setText(questions.get(count));

                    //Resetting the answer field
                    answer.setText("");
                    answer.setHint("Type your answer here");

                }else if(count == questions.size()-1){
                    //set the firstTime flag to false so the activity no longer opens for this account.
                    db.collection("users").document(currentUser.getEmail())
                            .update("firstTime", false)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    //All questions answered. Move onto the application.
                                    Intent intent = new Intent(getApplicationContext(), profileActivitiy.class);
                                    startActivity(intent);
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                }
                            });


                }
            }
        });
    }
}